import './product.dart';

class Order {
  final int state, date;
  final List<Product> products;

  Order(this.state, this.date, this.products);
}

List<Product> productsOrder1 = [pain, pain, muffinChocolat];
List<Product> productsOrder2 = [pain, muffinNature, muffinChocolat];
List<Product> productsOrder3 = [
  focaccia,
  brioche,
  brioche,
  brioche,
  brioche,
  muffinChocolat
];
List<Product> productsOrder4 = [
  focaccia,
  focaccia,
  focaccia,
  brioche,
  brioche,
  muffinChocolat,
  muffinNdc,
  muffinNdc,
  baguette
];
List<Order> orders = [
  Order(
    2,
    1609561761,
    productsOrder4,
  ),
  Order(
    3,
    1609061761,
    productsOrder1,
  ),
  Order(
    2,
    1609041321,
    productsOrder2,
  ),
  Order(
    3,
    1608061161,
    productsOrder3,
  ),
  Order(
    2,
    1605060161,
    productsOrder4,
  )
];
