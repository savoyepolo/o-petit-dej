class Product {
  final String id, name, description, picture, allergen, flour;
  final double price;
  final int calories, weight;

  Product(this.id, this.name, this.description, this.price, this.picture,
      this.calories, this.weight, this.allergen, this.flour);
}

final baguette = Product(
    "id",
    "Baguette de pain viennois",
    "Description baguette",
    1.9,
    "assets/images/baguette_viennois.png",
    230,
    70,
    "Gluten",
    "Blé");
final brioche = Product("id", "Brioche", "Description brioche", 6,
    "assets/images/brioche.png", 500, 90, "Gluten", "Blé");
final pain = Product(
    "id",
    "Le petit pain aux pavots du jardin",
    "Créé par demande spéciale. Notre Eden Burger Bun joliment garni de graines de lin dorées et de pavot",
    2.8,
    "assets/images/pain.png",
    245,
    90,
    "Gluten",
    "Sarasin");
final focaccia = Product("id", "Focaccia", "Description Foccacia", 2.2,
    "assets/images/focaccia.png", 250, 70, "Gluten", "Blé");
final muffinNature = Product("id", "Muffin nature", "Description muffin", 4.2,
    "assets/images/muffin_nature.png", 265, 45, "Gluten", "Blé");
final muffinChocolat = Product(
    "id",
    "Muffin chocolat",
    "Description muffin choco",
    4.4,
    "assets/images/muffin_chocolat.png",
    280,
    50,
    "Glutten",
    "Blé");
final muffinNdc = Product(
    "id",
    "Muffin noix de coco",
    "Description muffin noix de coco",
    4.3,
    "assets/images/muffin_ndc.png",
    275,
    50,
    "Glutten",
    "Blé");

/*List<Product> products = [
  pain,
  baguette,
  brioche,
  focaccia,
  muffinNature,
  muffinChocolat,
  muffinNdc
];*/
