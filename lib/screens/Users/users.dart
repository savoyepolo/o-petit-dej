import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../Profile/profile.dart';
import '../../provider/profile.dart';
import '../../services/image.dart';

class Users extends StatefulWidget {
  Users({Key key}) : super(key: key);

  @override
  _UsersState createState() => _UsersState();
}

class _UsersState extends State<Users> {
  @override
  Widget build(BuildContext context) {
    double coef = MediaQuery.of(context).size.width > 500 ? 2 : 1;

    final Stream<QuerySnapshot> _usersStream =
        FirebaseFirestore.instance.collection('users').snapshots();
    final profile = Provider.of<ProfileData>(context).data;
    return Container(
        child: Stack(children: [
      Positioned(
          top: 16,
          right: 16,
          child: GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Profile()));
              },
              child: profile['picture'] == null
                  ? CircleAvatar(
                      radius: 25.0,
                      backgroundImage:
                          AssetImage('assets/images/profile_picture.png'),
                      backgroundColor: Color(0xFFE94444))
                  : CircleAvatar(
                      radius: 25.0,
                      backgroundColor: Colors.white,
                      backgroundImage: NetworkImage(profile['picture'])))),
      Padding(
          padding: const EdgeInsets.only(top: 32, left: 32, right: 32),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Liste des utilisateurs',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25 * coef,
                  )),
              SizedBox(
                height: 32 * coef,
              ),
              Expanded(
                  child: StreamBuilder<QuerySnapshot>(
                stream: _usersStream,
                builder: (BuildContext context,
                    AsyncSnapshot<QuerySnapshot> snapshot) {
                  if (snapshot.hasError) {
                    return Text('Something went wrong');
                  }

                  if (snapshot.connectionState == ConnectionState.waiting) {
                    return Text("Loading");
                  }

                  return new ListView(
                    children:
                        snapshot.data.docs.map((DocumentSnapshot document) {
                      return new ListTile(
                        leading: FutureBuilder<String>(
                          future: downloadUserImage(
                              (document.data() as dynamic)['uid']),
                          builder: (BuildContext context,
                              AsyncSnapshot<String> snapshot) {
                            switch (snapshot.connectionState) {
                              case ConnectionState.waiting:
                                return Text(
                                  'Image Loading....',
                                  style: TextStyle(fontSize: 10),
                                );
                              default:
                                if (snapshot.hasError)
                                  return Text('Error: ${snapshot.error}');
                                else
                                  return CircleAvatar(
                                      backgroundColor: Colors.white,
                                      radius: 50.0,
                                      backgroundImage: NetworkImage(
                                          snapshot.data.toString()));
                            }
                          },
                        ),
                        title: new Text((document.data() as dynamic)['email']),
                        subtitle: new Text((document.data() as dynamic)['uid']),
                      );
                    }).toList(),
                  );
                },
              ))
            ],
          )),
    ]));
  }
}
