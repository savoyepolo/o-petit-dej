import 'dart:io';
import 'package:flutter/material.dart' hide Key;
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import '../../provider/profile.dart';
import '../../services/authentication.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Profile extends StatefulWidget {
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final picker = ImagePicker();
  String username;
  final _formKey = GlobalKey<FormState>();

  Map data = {
    'name': String,
    'firstname': String,
    'email': String,
    'picture': String,
  };

  Future openCamera() async {
    final pickedFile = await picker.getImage(source: ImageSource.camera);

    setState(() {
      if (pickedFile != null) {
        /*data['picture'] = File(pickedFile.path);
        _formKey.currentState.save();
        Provider.of<ProfileData>(context, listen: false).updateAccount(data);
        _formKey.currentState.reset();*/
        FirebaseStorage _storage = FirebaseStorage.instance;
        String filePath =
            'images/${context.read<Authentication>().getUserUid()}.png';
        _storage.ref().child(filePath).putFile(File(pickedFile.path));
      } else {
        print('No image selected.');
      }
    });

    Navigator.pop(context);
  }

  Future openGallery() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      if (pickedFile != null) {
        data['picture'] = File(pickedFile.path);
        _formKey.currentState.save();
        Provider.of<ProfileData>(context, listen: false).updateAccount(data);
        _formKey.currentState.reset();
        FirebaseStorage _storage = FirebaseStorage.instance;
        String filePath =
            'images/${context.read<Authentication>().getUserUid()}.png';
        _storage.ref().child(filePath).putFile(File(pickedFile.path));
      } else {
        print('No image selected.');
      }
    });
    Navigator.pop(context);
  }

  Future showChoiceDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
              title: Text('Choisir'),
              content: SingleChildScrollView(
                  child: ListBody(
                children: [
                  GestureDetector(
                      child: Text('Caméra'),
                      onTap: () {
                        openCamera();
                      }),
                  Padding(padding: EdgeInsets.all(8.0)),
                  GestureDetector(
                      child: Text('Gallerie'),
                      onTap: () {
                        openGallery();
                      }),
                ],
              )));
        });
  }

  @override
  Widget build(BuildContext context) {
    final userId = FirebaseAuth.instance.currentUser.uid;
    final Stream<DocumentSnapshot> _usersStream =
        FirebaseFirestore.instance.collection('users').doc(userId).snapshots();
    data = Provider.of<ProfileData>(context, listen: false).data;

    return StreamBuilder<DocumentSnapshot>(
        stream: _usersStream,
        builder:
            (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Text("Loading");
          }
          return Scaffold(
              body: Stack(
            children: [
              Container(
                decoration: new BoxDecoration(
                  image: new DecorationImage(
                    image: new AssetImage("assets/images/background.png"),
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              Positioned(
                  top: 16,
                  left: 16,
                  child: GestureDetector(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Image.asset('assets/images/back.png'))),
              Center(
                  child: Container(
                      width: MediaQuery.of(context).size.width * 0.85,
                      child: Form(
                          key: _formKey,
                          child: Column(
                              mainAxisSize: MainAxisSize.max,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [
                                GestureDetector(
                                    onTap: () {
                                      showChoiceDialog(context);
                                    },
                                    child: CircleAvatar(
                                        backgroundColor: Colors.white,
                                        radius: 50.0,
                                        backgroundImage:
                                            NetworkImage(data['picture']))),
                                TextFormField(
                                  autovalidateMode: AutovalidateMode.always,
                                  initialValue:
                                      snapshot.data['email'].toString(),
                                  onSaved: (String value) {
                                    data['email'] = value;
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Email',
                                    labelStyle: TextStyle(
                                      color: Color(0xFF757575),
                                    ),
                                    fillColor: Colors.white,
                                    filled: true,
                                  ),
                                ),
                                TextFormField(
                                  autovalidateMode: AutovalidateMode.always,
                                  initialValue:
                                      snapshot.data['name'].toString(),
                                  onSaved: (String value) {
                                    data['name'] = value;
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Nom',
                                    labelStyle: TextStyle(
                                      color: Color(0xFF757575),
                                    ),
                                    fillColor: Colors.white,
                                    filled: true,
                                  ),
                                ),
                                TextFormField(
                                  initialValue:
                                      snapshot.data['firstname'].toString(),
                                  onSaved: (String value) {
                                    data['firstname'] = value;
                                  },
                                  decoration: InputDecoration(
                                    labelText: 'Prénom',
                                    labelStyle: TextStyle(
                                      color: Color(0xFF757575),
                                    ),
                                    fillColor: Colors.white,
                                    filled: true,
                                  ),
                                ),
                                ButtonTheme(
                                    minWidth: 260.0,
                                    height: 50.0,
                                    child: RaisedButton(
                                      color: Color(0xFFE94444),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(50)),
                                      onPressed: () {
                                        _formKey.currentState.save();
                                        FirebaseFirestore.instance
                                            .collection("users")
                                            .doc(userId)
                                            .update({
                                          'name': data['name'],
                                          'firstname': data['firstname']
                                        });
                                        Provider.of<ProfileData>(context,
                                                listen: false)
                                            .updateAccount(data);
                                        _formKey.currentState.reset();
                                        Navigator.pop(context);
                                      },
                                      child: Text("Valider",
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 18,
                                            color: Colors.white,
                                          )),
                                    )),
                                GestureDetector(
                                  child: Text('Déconnexion',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18,
                                        color: Colors.white,
                                      )),
                                  onTap: () {
                                    context.read<Authentication>().signOut();
                                    Navigator.popUntil(
                                        context, ModalRoute.withName("/"));
                                  },
                                )
                              ]))))
            ],
          ));
        });
  }
}
