import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../provider/profile.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    double coef = MediaQuery.of(context).size.width > 500 ? 2 : 1;
    return Scaffold(
        body: Stack(children: [
      Container(
        decoration: new BoxDecoration(
          image: new DecorationImage(
            image: new AssetImage("assets/images/home.png"),
            fit: BoxFit.cover,
          ),
        ),
      ),
      Container(
        height: MediaQuery.of(context).size.height * 0.75,
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text('Ô Ptit dej',
                textAlign: TextAlign.center,
                style: TextStyle(
                    fontSize: 36 * coef,
                    fontWeight: FontWeight.bold,
                    color: Colors.white)),
            SizedBox(height: 15 * coef),
            Text('Le petit dej’ pour les grandes personnes',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 22 * coef, color: Colors.white)),
          ],
        ),
      ),
      Align(
        alignment: Alignment.bottomCenter,
        child: Padding(
            padding: const EdgeInsets.only(bottom: 20),
            child: ButtonTheme(
                minWidth: 260.0 * coef,
                height: 50.0 * coef,
                child: RaisedButton(
                  color: Color(0xFFE94444),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50)),
                  onPressed: () {
                    Provider.of<ProfileData>(context, listen: false).loadUser();
                    Navigator.pushNamed(context, '/menu');
                  },
                  child: Text("Régalez-moi !",
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18 * coef,
                        color: Colors.white,
                      )),
                ))),
      )
    ]));
  }
}
