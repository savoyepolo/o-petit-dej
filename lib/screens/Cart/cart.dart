import 'package:flutter/material.dart';
import 'package:optitdej/provider/cart.dart';
import 'package:provider/provider.dart';
import '../Profile/profile.dart';
import '../../models/order.dart';
import '../../provider/profile.dart';
import '../../provider/cart.dart';

class Cart extends StatefulWidget {
  @override
  _CartState createState() => _CartState();
}

class _CartState extends State<Cart> {
  var total = 0.0;
  doTotal(list) {
    total = 2.2;
    list.forEach((element) {
      total = total + element.price;
    });
    if (total == 2.2) total = 0;
  }

  int checkOccurences(list, element) {
    var foundElements = list.where((e) => e == element);
    return foundElements.length;
  }

  bool checkElement(list, element, index) {
    final firstIndex = list.indexWhere((e) => e.name == element.name);
    return (index > firstIndex ? false : true);
  }

  @override
  Widget build(BuildContext context) {
    double coef = MediaQuery.of(context).size.width > 500 ? 2 : 1;

    var order = Provider.of<CartData>(context).data;
    if (order.length != 0) doTotal(order);
    final profile = Provider.of<ProfileData>(context).data;

    return Container(
        width: MediaQuery.of(context).size.width,
        child: Stack(children: [
          Positioned(
              top: 16,
              right: 16,
              child: GestureDetector(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Profile()));
                  },
                  child: profile['picture'] == null
                      ? CircleAvatar(
                          radius: 25.0,
                          backgroundImage:
                              AssetImage('assets/images/profile_picture.png'),
                          backgroundColor: Color(0xFFE94444))
                      : CircleAvatar(
                          radius: 25.0,
                          backgroundColor: Colors.white,
                          backgroundImage: NetworkImage(profile['picture'])))),
          Padding(
              padding: const EdgeInsets.only(top: 32, left: 32, right: 32),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Panier',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 25 * coef,
                        )),
                    SizedBox(height: 25 * coef),
                    Text('Adresse de livraison',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18 * coef,
                        )),
                    SizedBox(height: 27 * coef),
                    Container(
                      child: TextFormField(
                        initialValue: '',
                        decoration: InputDecoration(
                          suffixIcon:
                              Icon(Icons.gps_fixed, color: Color(0xFF757575)),
                          focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.black),
                          ),
                          border: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.black),
                          ),
                        ),
                      ),
                    ),
                    SizedBox(height: 29 * coef),
                    Text('Moyen de paiement',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18 * coef,
                        )),
                    SizedBox(height: 12 * coef),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Image.asset('assets/images/paypal.png',
                            height: 35 * coef),
                        Image.asset('assets/images/cb.png', height: 35 * coef),
                        Image.asset('assets/images/paylib.png',
                            height: 35 * coef),
                      ],
                    ),
                    SizedBox(height: 12 * coef),
                    Text('Votre commande',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18 * coef,
                        )),
                    Expanded(
                        child: ListView.builder(
                            itemCount: order != null ? order.length : 0,
                            physics: ClampingScrollPhysics(),
                            shrinkWrap: true,
                            itemBuilder: (context, index) {
                              if (order != null &&
                                  checkElement(order, order[index], index)) {
                                return (Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                  children: [
                                    Text(
                                        '${checkOccurences(order, order[index])}x',
                                        style: TextStyle(
                                            fontSize: 14 * coef,
                                            fontWeight: FontWeight.bold)),
                                    Image.network(order[index].picture,
                                        height: 85 * coef),
                                    Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.end,
                                        children: [
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.45,
                                            child: Text('${order[index].name}',
                                                style: TextStyle(
                                                    fontSize: 14 * coef)),
                                          ),
                                          Container(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                0.45,
                                            child: Text(
                                                '${order[index].price.toStringAsFixed(2)} €',
                                                style: TextStyle(
                                                    fontSize: 14 * coef)),
                                          )
                                        ]),
                                    GestureDetector(
                                      onTap: () {
                                        Provider.of<CartData>(context,
                                                listen: false)
                                            .subToCart(order[index]);
                                        doTotal(order);
                                      },
                                      child: Icon(Icons.remove_circle_outline,
                                          size: 24 * coef,
                                          color: Color(0xFF757575)),
                                    )
                                  ],
                                ));
                              } else
                                return (SizedBox.shrink());
                            })),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Text('Livraison',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14 * coef,
                                      color: Color(0xFF757575))),
                            ],
                          ),
                          Column(
                            children: [
                              Text('2,20 €',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14 * coef,
                                      color: Color(0xFF757575))),
                            ],
                          )
                        ]),
                    Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Column(
                            children: [
                              Text('Total',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 18 * coef,
                                      color: Color(0xFF757575))),
                            ],
                          ),
                          Column(
                            children: [
                              Text('${total.toStringAsFixed(2)} €',
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14 * coef,
                                      color: Color(0xFF757575))),
                            ],
                          )
                        ]),
                    SizedBox(height: 20 * coef),
                    Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: [
                            ButtonTheme(
                                minWidth: 260.0 * coef,
                                height: 50.0 * coef,
                                child: RaisedButton(
                                  color: Color(0xFFE94444),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50)),
                                  onPressed: () {
                                    orders.insert(
                                        0,
                                        Order(
                                            1,
                                            DateTime.now()
                                                    .millisecondsSinceEpoch ~/
                                                1000,
                                            order));
                                    debugPrint("Paiement");
                                  },
                                  child: Text(
                                      "Payer (${total.toStringAsFixed(2)} €)",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18 * coef,
                                        color: Colors.white,
                                      )),
                                ))
                          ],
                        )),
                    SizedBox(height: 16 * coef)
                  ])),
        ]));
  }
}
