import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../../models/product.dart';
import '../../provider/cart.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import './editProduct.dart';

class Details extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    double coef = MediaQuery.of(context).size.width > 500 ? 2 : 1;
    final ProductDetailsArguments agrs =
        ModalRoute.of(context).settings.arguments;

    final Stream<DocumentSnapshot> _editProduct = FirebaseFirestore.instance
        .collection('users_rights')
        .doc(FirebaseAuth.instance.currentUser.uid + "_gGpz8yzMzrTVicUyhsa0z")
        .snapshots();

    return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Stack(
          children: [
            Positioned(
                top: 16,
                left: 16,
                child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Image.asset('assets/images/back_grey.png'))),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                    padding: const EdgeInsets.only(top: 68, left: 32),
                    child: Container(
                        width: MediaQuery.of(context).size.width * 0.75,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(agrs.product.name,
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 25 * coef)),
                              SizedBox(height: 10),
                              Text(
                                agrs.product.description,
                                style: TextStyle(
                                    fontSize: 14 * coef,
                                    color: Color(0xFF757575)),
                              )
                            ]))),
                Padding(
                    padding: const EdgeInsets.only(top: 21, left: 32),
                    child: Container(
                        width: MediaQuery.of(context).size.width * 0.45,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text('Calories',
                                  style: TextStyle(
                                      fontSize: 16 * coef,
                                      color: Color(0xFF757575))),
                              Text(
                                '${agrs.product.calories} kCal',
                                style: TextStyle(
                                    fontSize: 18 * coef,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 30),
                              Text('Poids',
                                  style: TextStyle(
                                      fontSize: 16 * coef,
                                      color: Color(0xFF757575))),
                              Text(
                                '${agrs.product.weight} g',
                                style: TextStyle(
                                    fontSize: 18 * coef,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 30),
                              Text('Allergènes',
                                  style: TextStyle(
                                      fontSize: 16 * coef,
                                      color: Color(0xFF757575))),
                              Text(
                                '${agrs.product.allergen}',
                                style: TextStyle(
                                    fontSize: 18 * coef,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 30),
                              Text('Farine',
                                  style: TextStyle(
                                      fontSize: 16 * coef,
                                      color: Color(0xFF757575))),
                              Text(
                                '${agrs.product.flour}',
                                style: TextStyle(
                                    fontSize: 18 * coef,
                                    fontWeight: FontWeight.bold),
                              ),
                              SizedBox(height: 30),
                              Text('Prix',
                                  style: TextStyle(
                                      fontSize: 16 * coef,
                                      color: Color(0xFF757575))),
                              Text(
                                '${agrs.product.price.toStringAsFixed(2)} €',
                                style: TextStyle(
                                    fontSize: 18 * coef,
                                    fontWeight: FontWeight.bold),
                              ),
                            ]))),
              ],
            ),
            Positioned(
                right: -250,
                top: 200,
                child: Image.network(agrs.product.picture,
                    height: coef == 2 ? 600 : 400, fit: BoxFit.fitWidth)),
            Align(
                alignment: Alignment.bottomCenter,
                child: StreamBuilder<DocumentSnapshot>(
                    stream: _editProduct,
                    builder: (BuildContext context,
                        AsyncSnapshot<DocumentSnapshot> snapshot) {
                      if (snapshot.hasError) {
                        return Text(snapshot.error.toString());
                      }
                      if (snapshot.connectionState == ConnectionState.waiting) {
                        return Scaffold(
                            body: Center(child: Text("Loading...")));
                      }
                      return Column(mainAxisSize: MainAxisSize.min, children: [
                        snapshot.data.exists
                            ? Padding(
                                padding: const EdgeInsets.only(bottom: 20),
                                child: ButtonTheme(
                                    minWidth: 30,
                                    height: 50.0 * coef,
                                    child: RaisedButton(
                                      color: Color(0xFFE94444),
                                      shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(50)),
                                      onPressed: () {
                                        showDialog(
                                          context: context,
                                          builder: (BuildContext context) =>
                                              EditProduct(id: agrs.product.id),
                                        );
                                      },
                                      child: Icon(
                                        Icons.edit,
                                        color: Colors.white,
                                      ),
                                    )))
                            : SizedBox(),
                        Padding(
                            padding: const EdgeInsets.only(bottom: 20),
                            child: ButtonTheme(
                                minWidth: 260.0 * coef,
                                height: 50.0 * coef,
                                child: RaisedButton(
                                  color: Color(0xFFE94444),
                                  shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50)),
                                  onPressed: () {
                                    Provider.of<CartData>(context,
                                            listen: false)
                                        .addToCart(agrs.product);
                                    Navigator.pop(context);
                                  },
                                  child: Text("Ajouter au panier",
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 18 * coef,
                                        color: Colors.white,
                                      )),
                                ))),
                      ]);
                    }))
          ],
        ));
  }
}

class ProductDetailsArguments {
  final Product product;

  ProductDetailsArguments({this.product});
}
