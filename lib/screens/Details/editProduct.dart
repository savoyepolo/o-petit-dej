import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class EditProduct extends StatelessWidget {
  final String id;

  EditProduct({this.id});

  final TextEditingController descriptionController = TextEditingController();
  final TextEditingController priceController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return new AlertDialog(
      title: const Text('Modifier Produit'),
      content: new Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            TextField(
              controller: descriptionController,
              decoration: InputDecoration(
                labelText: 'Description',
                labelStyle: TextStyle(
                  color: Color(0xFF757575),
                ),
                fillColor: Colors.white,
                filled: true,
              ),
            ),
            TextField(
              controller: priceController,
              decoration: InputDecoration(
                labelText: 'Prix',
                labelStyle: TextStyle(
                  color: Color(0xFF757575),
                ),
                fillColor: Colors.white,
                filled: true,
              ),
            ),
          ]),
      actions: [
        new ElevatedButton(
          style: ElevatedButton.styleFrom(
              primary: Color(0xFFE94444),
              onPrimary: Colors.white,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(50))),
          onPressed: () {
            FirebaseFirestore.instance
                .collection("products")
                .doc(this.id)
                .update({
              'description': descriptionController.text.trim(),
              'price': double.parse(priceController.text.trim())
            });
            Navigator.of(context).pop();
          },
          child: const Text('Valider'),
        ),
      ],
    );
  }
}
