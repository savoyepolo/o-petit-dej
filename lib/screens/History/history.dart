import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../Profile/profile.dart';
import '../../models/order.dart';
import '../../provider/profile.dart';

class History extends StatefulWidget {
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  var total = 0.0;
  doTotal(list) {
    total = 2.2;
    list.forEach((element) {
      total = total + element.price;
    });
  }

  int checkOccurences(list, element) {
    var foundElements = list.where((e) => e == element);
    return foundElements.length;
  }

  bool checkElement(list, element, index) {
    final firstIndex = list.indexWhere((e) => e.name == element.name);
    return (index > firstIndex ? false : true);
  }

  bool checkOrder(element) {
    return (element.state == 0 ? false : true);
  }

  @override
  Widget build(BuildContext context) {
    double coef = MediaQuery.of(context).size.width > 500 ? 2 : 1;

    final profile = Provider.of<ProfileData>(context).data;

    return Container(
        child: Stack(children: [
      Positioned(
          top: 16,
          right: 16,
          child: GestureDetector(
              onTap: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Profile()));
              },
              child: profile['picture'] == null
                  ? CircleAvatar(
                      radius: 25.0,
                      backgroundImage:
                          AssetImage('assets/images/profile_picture.png'),
                      backgroundColor: Color(0xFFE94444))
                  : CircleAvatar(
                      radius: 25.0,
                      backgroundColor: Colors.white,
                      backgroundImage: NetworkImage(profile['picture'])))),
      Padding(
          padding: const EdgeInsets.only(top: 32, left: 32, right: 32),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text('Mes commandes',
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 25 * coef,
                  )),
              orders != null
                  ? Expanded(
                      child: ListView.builder(
                          itemCount: orders != null ? orders.length : 0,
                          itemBuilder: (context, index) {
                            var order = orders[index];
                            if (checkOrder(order)) {
                              var date =
                                  new DateTime.fromMillisecondsSinceEpoch(
                                      order.date * 1000);
                              var state = order.state == 1
                                  ? 'en cours'
                                  : order.state == 2
                                      ? 'terminée'
                                      : 'annulée';
                              doTotal(order.products);

                              return (Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    SizedBox(
                                      height: 32 * coef,
                                    ),
                                    RichText(
                                      text: TextSpan(
                                        children: [
                                          WidgetSpan(
                                            child: Icon(
                                              order.state == 1
                                                  ? Icons
                                                      .hourglass_empty_outlined
                                                  : order.state == 2
                                                      ? Icons
                                                          .check_circle_outline
                                                      : Icons
                                                          .highlight_off_outlined,
                                              color: order.state == 1
                                                  ? Colors.orange
                                                  : order.state == 2
                                                      ? Colors.green
                                                      : Colors.red,
                                              size: 20 * coef,
                                            ),
                                          ),
                                          TextSpan(
                                              text:
                                                  ' Commande $state du ${date.day}/${date.month}/${date.year}',
                                              style: TextStyle(
                                                  fontSize: 16 * coef,
                                                  color: Colors.black)),
                                        ],
                                      ),
                                    ),
                                    SizedBox(height: 4 * coef),
                                    Container(
                                        color: Colors.black,
                                        height: 1 * coef,
                                        width:
                                            MediaQuery.of(context).size.width *
                                                0.8),
                                    SizedBox(height: 10 * coef),
                                    ListView.builder(
                                        itemCount: order.products.length,
                                        physics: ClampingScrollPhysics(),
                                        shrinkWrap: true,
                                        itemBuilder: (context, i) {
                                          if (checkElement(order.products,
                                              order.products[i], i)) {
                                            return (Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.start,
                                                children: [
                                                  Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .start,
                                                      children: [
                                                        Text(
                                                            '${checkOccurences(order.products, order.products[i])}x',
                                                            style: TextStyle(
                                                                fontSize:
                                                                    18 * coef,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .bold,
                                                                color: Color(
                                                                    0xFF757575))),
                                                        SizedBox(
                                                            height: 20 * coef),
                                                      ]),
                                                  SizedBox(width: 15 * coef),
                                                  Column(
                                                      crossAxisAlignment:
                                                          CrossAxisAlignment
                                                              .end,
                                                      children: [
                                                        Text(
                                                            '${order.products[i].name}',
                                                            style: TextStyle(
                                                                fontSize:
                                                                    16 * coef,
                                                                color: Color(
                                                                    0xFF757575))),
                                                        SizedBox(
                                                            height: 20 * coef),
                                                      ])
                                                ]));
                                          } else
                                            return (SizedBox.shrink());
                                        }),
                                    Text('Total ${total.toStringAsFixed(2)} €',
                                        style: TextStyle(
                                            fontSize: 16 * coef,
                                            fontWeight: FontWeight.bold))
                                  ]));
                            } else
                              return (SizedBox.shrink());
                          }))
                  : Container(
                      height: MediaQuery.of(context).size.height * 0.75,
                      width: MediaQuery.of(context).size.width * 0.75,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Text("Aucune commande dans l'historique",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                  fontSize: 20 * coef,
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xFF757575))),
                          Icon(Icons.outlet_outlined, color: Color(0xFF757575))
                        ],
                      ))
            ],
          )),
    ]));
  }
}
