import '../../services/authentication.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SignUp extends StatelessWidget {
  final TextEditingController firstnameController = TextEditingController();
  final TextEditingController nameController = TextEditingController();
  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    double coef = MediaQuery.of(context).size.width > 500 ? 2 : 1;
    return Scaffold(
        body: Stack(
      children: [
        Container(
          decoration: new BoxDecoration(
            image: new DecorationImage(
              image: new AssetImage("assets/images/background.png"),
              fit: BoxFit.cover,
            ),
          ),
        ),
        Positioned(
            top: 16,
            left: 16,
            child: GestureDetector(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Image.asset('assets/images/back.png'))),
        Center(
            child: Container(
                width: MediaQuery.of(context).size.width * 0.85,
                child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Text('Inscription',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              fontSize: 36 * coef,
                              fontWeight: FontWeight.bold,
                              color: Colors.white)),
                      TextField(
                        controller: firstnameController,
                        decoration: InputDecoration(
                          labelText: 'Prénom',
                          labelStyle: TextStyle(
                            color: Color(0xFF757575),
                          ),
                          fillColor: Colors.white,
                          filled: true,
                        ),
                      ),
                      TextField(
                        controller: nameController,
                        decoration: InputDecoration(
                          labelText: 'Nom',
                          labelStyle: TextStyle(
                            color: Color(0xFF757575),
                          ),
                          fillColor: Colors.white,
                          filled: true,
                        ),
                      ),
                      TextField(
                        controller: emailController,
                        decoration: InputDecoration(
                          labelText: 'Email',
                          labelStyle: TextStyle(
                            color: Color(0xFF757575),
                          ),
                          fillColor: Colors.white,
                          filled: true,
                        ),
                      ),
                      TextField(
                        obscureText: true,
                        controller: passwordController,
                        decoration: InputDecoration(
                          labelText: 'Mot de passe',
                          labelStyle: TextStyle(
                            color: Color(0xFF757575),
                          ),
                          fillColor: Colors.white,
                          filled: true,
                        ),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            primary: Color(0xFFE94444),
                            onPrimary: Colors.white,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(50)),
                            minimumSize: Size(260, 50)),
                        onPressed: () {
                          context.read<Authentication>().signUp(
                                email: emailController.text.trim(),
                                firstname: firstnameController.text.trim(),
                                name: nameController.text.trim(),
                                password: passwordController.text.trim(),
                              );
                          Navigator.pop(context);
                        },
                        child: Text("Valider",
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              fontSize: 18,
                              color: Colors.white,
                            )),
                      ),
                    ])))
      ],
    ));
  }
}
