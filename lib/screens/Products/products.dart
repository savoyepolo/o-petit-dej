import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../Details/details.dart';
import '../Profile/profile.dart';
import '../../models/product.dart';
import '../../provider/profile.dart';
import '../../services/image.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class ChooseProduct extends StatefulWidget {
  @override
  _ChooseProductState createState() => _ChooseProductState();
}

class _ChooseProductState extends State<ChooseProduct> {
  @override
  Widget build(BuildContext context) {
    double coef = MediaQuery.of(context).size.width > 500 ? 2 : 1;
    final profile = Provider.of<ProfileData>(context).data;

    final Stream<QuerySnapshot> _readProduct =
        FirebaseFirestore.instance.collection('products').snapshots();

    return Container(
        child: Stack(
      children: [
        Positioned(
            top: 16,
            right: 16,
            child: GestureDetector(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Profile()));
                },
                child: profile['picture'] == null
                    ? CircleAvatar(
                        radius: 25.0,
                        backgroundImage:
                            AssetImage('assets/images/profile_picture.png'),
                        backgroundColor: Color(0xFFE94444))
                    : CircleAvatar(
                        radius: 25.0,
                        backgroundColor: Colors.white,
                        backgroundImage: NetworkImage(profile['picture'])))),
        Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
              padding: const EdgeInsets.only(top: 40, left: 32),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      'Bonjour ' + profile['firstname'].toString() + ',',
                      style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 12 * coef,
                          color: Color(0xFF757575)),
                    ),
                    Container(
                        width: MediaQuery.of(context).size.width * 0.65,
                        child: Text(
                          'Que voulez-vous manger ce matin ?',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 25 * coef),
                        )),
                  ])),
          Expanded(
              child: StreamBuilder<QuerySnapshot>(
                  stream: _readProduct,
                  builder: (BuildContext context,
                      AsyncSnapshot<QuerySnapshot> snapshot) {
                    if (snapshot.hasError) {
                      return Text('Something went wrong');
                    }

                    if (snapshot.connectionState == ConnectionState.waiting) {
                      return Text("Loading");
                    }
                    return new ListView(
                        children:
                            snapshot.data.docs.map((DocumentSnapshot document) {
                      return FutureBuilder<String>(
                          future: downloadProductImage(
                              (document.data() as dynamic)['id']),
                          builder: (BuildContext context,
                              AsyncSnapshot<String> snapshot) {
                            switch (snapshot.connectionState) {
                              case ConnectionState.waiting:
                                return Text(
                                  'Image Loading....',
                                  style: TextStyle(fontSize: 10),
                                );
                              default:
                                if (snapshot.hasError)
                                  return Text('Error: ${snapshot.error}');
                                else
                                  return GestureDetector(
                                      onTap: () => Navigator.pushNamed(
                                          context, '/details',
                                          arguments: ProductDetailsArguments(
                                              product: Product(
                                                  document.data()['id'],
                                                  document.data()['name'],
                                                  document
                                                      .data()['description'],
                                                  (document.data()['price'])
                                                      .toDouble(),
                                                  snapshot.data.toString(),
                                                  document.data()['calories'],
                                                  document.data()['weight'],
                                                  document.data()['allergen'],
                                                  document.data()['flour']))),
                                      child: Padding(
                                          padding: const EdgeInsets.only(
                                            left: 32,
                                          ),
                                          child: Row(
                                            children: [
                                              Image.network(
                                                  snapshot.data.toString(),
                                                  width: 100),
                                              Column(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: [
                                                  Padding(
                                                      padding:
                                                          const EdgeInsets.only(
                                                        top: 16,
                                                      ),
                                                      child: Text(
                                                          document
                                                              .data()['name'],
                                                          style: TextStyle(
                                                            fontSize: 16 * coef,
                                                          ))),
                                                  Text(
                                                      document
                                                              .data()['price']
                                                              .toStringAsFixed(
                                                                  2) +
                                                          '€',
                                                      style: TextStyle(
                                                        color:
                                                            Color(0xFF757575),
                                                        fontSize: 16 * coef,
                                                      ))
                                                ],
                                              )
                                            ],
                                          )));
                            }
                          });
                    }).toList());
                  })),
        ]),
      ],
    ));
  }
}
