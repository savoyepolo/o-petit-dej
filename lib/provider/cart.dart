import 'package:flutter/material.dart';
import '../models/product.dart';

class CartData extends ChangeNotifier {
  List<Product> data = [];

  void addToCart(input) {
    data.insert(0, input);
    notifyListeners();
  }

  void subToCart(input) {
    data.remove(input);
    notifyListeners();
  }
}
