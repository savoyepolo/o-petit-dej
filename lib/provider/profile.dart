import 'package:flutter/material.dart' hide Key;
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import '../services/image.dart';

class ProfileData extends ChangeNotifier {
  Map data = {};

  void loadUser() {
    final userId = FirebaseAuth.instance.currentUser.uid;
    FirebaseFirestore.instance
        .collection('users')
        .doc(userId)
        .get()
        .then((result) => {
              {
                if (result.exists)
                  {
                    data = result.data(),
                    downloadUserImage(data['uid'])
                        .then((result) => data['picture'] = result),
                    notifyListeners()
                  }
              }
            });
  }

  void updateAccount(input) {
    data = input;
    downloadUserImage(data['uid']).then((result) => data['picture'] = result);
    notifyListeners();
  }
}
