import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import '../screens/Products/products.dart';
import '../screens/Cart/cart.dart';
import '../screens/History/history.dart';
import '../screens/Users/users.dart';

class Menu extends StatefulWidget {
  Menu({Key key}) : super(key: key);

  @override
  _MenuState createState() => _MenuState();
}

class _MenuState extends State<Menu> {
  /*bool readUser = false;

  @override
  void initState() {
    super.initState();
    FirebaseFirestore.instance
        .collection('users_rights')
        .doc(FirebaseAuth.instance.currentUser.uid + "_NiFkHfhCjzHxefRkYpfa")
        .get()
        .then((result) => {
              {
                if (result.exists)
                  setState(() {
                    readUser = true;
                  })
              }
            });
  }*/

  int _selectedIndex = 0;
  static List<Widget> _widgetOptions = <Widget>[
    ChooseProduct(),
    Cart(),
    History(),
    Users(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    double coef = MediaQuery.of(context).size.width > 500 ? 2 : 1;

    final Stream<DocumentSnapshot> _readUser = FirebaseFirestore.instance
        .collection('users_rights')
        .doc(FirebaseAuth.instance.currentUser.uid + "_NiFkHfhCjzHxefRkYpfa")
        .snapshots();

    List<BottomNavigationBarItem> items = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.list),
        label: 'Produits',
        backgroundColor: Color(0xFFE94444),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.shopping_cart),
        label: 'Panier',
        backgroundColor: Color(0xFFE94444),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.history),
        label: 'Commandes',
        backgroundColor: Color(0xFFE94444),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.supervisor_account),
        label: 'Utilisateurs',
        backgroundColor: Color(0xFFE94444),
      ),
    ];

    List<BottomNavigationBarItem> items1 = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: Icon(Icons.list),
        label: 'Produits',
        backgroundColor: Color(0xFFE94444),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.shopping_cart),
        label: 'Panier',
        backgroundColor: Color(0xFFE94444),
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.history),
        label: 'Commandes',
        backgroundColor: Color(0xFFE94444),
      ),
    ];

    return StreamBuilder<DocumentSnapshot>(
        stream: _readUser,
        builder:
            (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
          if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          if (snapshot.connectionState == ConnectionState.waiting) {
            return Scaffold(body: Center(child: Text("Loading...")));
          }
          return Scaffold(
            body: Center(
              child: _widgetOptions.elementAt(_selectedIndex),
            ),
            bottomNavigationBar: BottomNavigationBar(
              selectedFontSize: 14 * coef,
              iconSize: 24 * coef,
              currentIndex: _selectedIndex,
              selectedItemColor: Colors.white,
              unselectedItemColor: Colors.white,
              showUnselectedLabels: false,
              backgroundColor: Color(0xFFE94444),
              items: snapshot.data.exists ? items : items1,
              onTap: _onItemTapped,
            ),
          );
        });
  }
}
