import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Authentication {
  final FirebaseAuth _firebaseAuth;

  Authentication(this._firebaseAuth);

  Stream<User> get authStateChanges => _firebaseAuth.authStateChanges();

  String getUserUid() {
    final User user = _firebaseAuth.currentUser;
    final uid = user.uid;
    return uid;
  }

  Future<void> signOut() async {
    await _firebaseAuth.signOut();
  }

  Future<String> signIn({String email, String password}) async {
    try {
      await _firebaseAuth.signInWithEmailAndPassword(
          email: email, password: password);
      return "Signed In";
    } on FirebaseAuthException catch (e) {
      return e.message;
    }
  }

  Future<String> signUp(
      {String email, String password, String name, String firstname}) async {
    try {
      UserCredential result = await _firebaseAuth
          .createUserWithEmailAndPassword(email: email, password: password);
      User user = result.user;
      await FirebaseFirestore.instance.collection('users').doc(user?.uid).set({
        'email': email,
        'firstname': firstname,
        'name': name,
        'uid': user?.uid
      });

      return "Signed Up";
    } on FirebaseAuthException catch (e) {
      return e.message;
    }
  }
}
