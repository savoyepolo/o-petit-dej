import 'package:firebase_storage/firebase_storage.dart' as firebase_storage;

Future<String> downloadUserImage(id) async {
  String downloadURL = "";
  try {
    downloadURL = await firebase_storage.FirebaseStorage.instance
        .ref('images/' + id + '.png')
        .getDownloadURL();
    return (downloadURL);
  } catch (onError) {
    return ("https://cdn4.iconfinder.com/data/icons/small-n-flat/24/user-alt-512.png");
  }
}

Future<String> downloadProductImage(id) async {
  String downloadURL = "";
  try {
    downloadURL = await firebase_storage.FirebaseStorage.instance
        .ref('products/' + id + '.png')
        .getDownloadURL();
    return (downloadURL);
  } catch (onError) {
    return ("");
  }
}
