import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'screens/Users/users.dart';
import 'screens/Details/details.dart';
import 'screens/Products/products.dart';
import 'screens/SignIn/signin.dart';
import 'screens/Home/home.dart';
import 'provider/profile.dart';
import 'provider/cart.dart';
import 'widgets/menu.dart';
import 'services/authentication.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(App());
}

class AuthenticationWrapper extends StatelessWidget {
  const AuthenticationWrapper({
    Key key,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    final firebaseUser = context.watch<User>();

    if (firebaseUser != null) {
      return Home();
    }
    return SignIn();
  }
}

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIOverlays([SystemUiOverlay.bottom]);
    return MultiProvider(
        providers: [
          ChangeNotifierProvider.value(
            value: ProfileData(),
          ),
          ChangeNotifierProvider.value(
            value: CartData(),
          ),
          Provider<Authentication>(
            create: (_) => Authentication(FirebaseAuth.instance),
          ),
          StreamProvider(
            create: (context) =>
                context.read<Authentication>().authStateChanges,
          )
        ],
        child: MaterialApp(
          title: 'O ptit dej',
          theme: ThemeData(
              primaryColor: Color(0xFFE94444),
              scaffoldBackgroundColor: Colors.white,
              fontFamily: 'NotoSerif'),
          home: AuthenticationWrapper(),
          routes: {
            '/products': (context) => ChooseProduct(),
            '/menu': (context) => Menu(),
            '/details': (context) => Details(),
            '/users': (context) => Users(),
          },
        ));
  }
}
