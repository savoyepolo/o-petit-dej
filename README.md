# O'Ptit Dej'

A Flutter application representing a home
breakfast delivery service. The user has the
choice among a set of products and can, after
selection, order and be delivered at home.
